using System;

namespace RapidRoom
{
	public enum MessageType
	{
		Authenticate = 1,	// IN
		ListRooms,			// IN
		RoomsList,			// OUT
		JoinRoom,			// IN
		PeopleList,			// OUT
		PersonIncoming,		// OUT
		PersonLeaving,		// OUT
		SendMessage,		// IN
		IncomingMessage		// OUT
	}
}

