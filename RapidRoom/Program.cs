using System;

namespace RapidRoom
{
	class MainClass
	{
		public static void Main(string[] args) {
			new Server(8181).Start();
			Console.ReadKey(false);
		}
	}
}
