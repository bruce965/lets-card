using System;
using System.Collections.Generic;
using System.Linq;

namespace RapidRoom
{
	public class Room
	{
		public readonly string Name;

		private readonly Server server;

		private object peopleLock = new object();
		private readonly Dictionary<string, Person> people = new Dictionary<string, Person>();

		public Room(Server server, string name) {
			this.server = server;
			this.Name = name;
		}

		/// <returns>People before adding.</returns>
		public Person[] AddPerson(Person person) {
			Person[] previous;
			lock(peopleLock) {
				previous = people.Values.ToArray();
				people[person.Name] = person;
			}

			Person.SendMessage(previous, MessageType.PersonIncoming, new PersonData() { name = person.Name });
			return previous;
		}

		public Person[] GetPeopleExcept(Person person) {
			lock(peopleLock) {
				return people.Values.Except(new [] { person }).ToArray();
			}
		}

		public void RemovePerson(Person person) {
			Person[] after;
			lock(peopleLock) {
				people.Remove(person.Name);

				if(people.Count == 0) {
					server.DestroyRoom(this);
					return;
				}

				after = people.Values.ToArray();
			}

			Person.SendMessage(after, MessageType.PersonLeaving, new PersonData() { name = person.Name });
		}
	}
}

