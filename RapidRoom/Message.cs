using System;

namespace RapidRoom
{
	public class Message<T>
	{
		public int type;
		public T data;

		public Message(MessageType type, T data) {
			this.type = (int)type;
			this.data = data;
		}
	}
}
