using System;
using fastJSON;
using Fleck;
using System.Linq;

namespace RapidRoom
{
	public class Person
	{
		public string Name { get; private set; }
		public Room Room { get; private set; }

		private readonly Server server;
		private readonly IWebSocketConnection sock;

		public Person(Server server, IWebSocketConnection sock) {
			this.server = server;
			this.sock = sock;

			Console.WriteLine("Person connected!", Name);	// DEBUG
		}

		public void ProcessMessage(MessageType type, string json) {
			if(type == MessageType.Authenticate) {
				authenticate(parseJson<Authentication>(json));
				return;
			}

			if(Name == null)
				return;	// not authenticated

			switch(type) {
			case MessageType.JoinRoom:
				joinRoom(parseJson<JoinRoom>(json));
				break;
			case MessageType.ListRooms:
				listRooms();
				break;
			case MessageType.SendMessage:
				sendMessage(parseJson<SendMessage>(json));
				break;
			}
		}

		public static void SendMessage<T>(Person[] people, MessageType type, T data) {
			if(people.Length == 0)
				return;

			var msg = new Message<T>(type, data);
			foreach(var person in people)
				person.SendMessage(msg);
		}

		public void SendMessage<T>(MessageType type, T data) {
			SendMessage(new Message<T>(type, data));
		}

		public void SendMessage<T>(Message<T> msg) {
			sock.Send(JSON.ToJSON(msg));
		}

		public void Disconnected() {
			Console.WriteLine("{0} disconnected!", Name == null ? "Unauthenticated person" : ("\"" + Name + "\""));	// DEBUG
			
			if(Room != null)
				Room.RemovePerson(this);
		}

		private void authenticate(Authentication auth) {
			if(Name != null)
				return;	// already authenticated

			this.Name = auth.name;
			Console.WriteLine("\"{0}\" authenticated!", Name);	// DEBUG
		}

		private void joinRoom(JoinRoom join) {
			if(Room != null)
				Room.RemovePerson(this);

			Room = server.GetRoom(join.room);
			var others = Room.AddPerson(this);

			SendMessage(MessageType.PeopleList, new PeopleData() {
				people = others.Select(person => person.Name).ToArray()
			});
		}

		private void listRooms() {
			SendMessage(MessageType.RoomsList, new RoomsData() {
				rooms = server.GetRoomNames()
			});
		}

		private void sendMessage(SendMessage message) {
			if(Room == null)
				return;

			SendMessage(Room.GetPeopleExcept(this), MessageType.IncomingMessage, new IncomingMessage() {
				content = message.message,
				sender = Name
			});
		}

		private T parseJson<T>(string json) where T : new() {
			return (T)JSON.FillObject(new T(), json);
		}
	}
}

