using System;
using Fleck;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;

namespace RapidRoom
{
	public class Server
	{
		private readonly WebSocketServer server;

		private object roomsLock = new object();
		private readonly Dictionary<string, Room> rooms = new Dictionary<string, Room>();

		public Server(ushort port) {
			server = new WebSocketServer(port, "ws://0");
		}

		public void Start() {
			server.Start(socket => {
				var person = new Person(this, socket);
				//socket.OnOpen = () => Console.WriteLine("Open!");
				socket.OnClose = () => person.Disconnected();
				//socket.OnError = exception => Console.WriteLine("Error!");
				socket.OnMessage = message => messageIncoming(person, message);
			});
		}

		public void Stop() {
			server.Dispose();
		}

		public Room GetRoom(string name) {
			lock(roomsLock) {
				Room room;
				if(rooms.TryGetValue(name, out room))
					return room;

				return rooms[name] = new Room(this, name);
			}
		}

		public string[] GetRoomNames() {
			lock(roomsLock) {
				return rooms.Values.Select(room => room.Name).ToArray();
			}
		}

		public void DestroyRoom(Room room) {
			lock(roomsLock) {
				rooms.Remove(room.Name);
			}
		}

		private void messageIncoming(Person person, string message) {
			//Console.WriteLine("Message incoming: "+message);	// DEBUG

			var separator = message.IndexOf('~', 0, Math.Min(10, message.Length));
			if(separator < 0)
				return;	// no separator

			int messageType;
			if(!int.TryParse(message.Substring(0, separator), out messageType))
				return;	// no valid message type

			string json = message.Substring(separator + 1);
			person.ProcessMessage((MessageType)messageType, json);
		}
	}
}

