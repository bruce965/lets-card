var letscard;
(function (letscard) {
    var MessageType;
    (function (MessageType) {
        MessageType[MessageType["authenticate"] = 1] = "authenticate";
        MessageType[MessageType["listRooms"] = 2] = "listRooms";
        MessageType[MessageType["roomsList"] = 3] = "roomsList";
        MessageType[MessageType["joinRoom"] = 4] = "joinRoom";
        MessageType[MessageType["peopleList"] = 5] = "peopleList";
        MessageType[MessageType["personIncoming"] = 6] = "personIncoming";
        MessageType[MessageType["personLeaving"] = 7] = "personLeaving";
        MessageType[MessageType["sendMessage"] = 8] = "sendMessage";
        MessageType[MessageType["incomingMessage"] = 9] = "incomingMessage"; // IN
    })(MessageType || (MessageType = {}));
    var Networking = (function () {
        function Networking(endpoint, onConnected, onNewRooms, onNewPeople, onMessage, onJoinRoom, onDisconnected) {
            var _this = this;
            this.onConnected = onConnected;
            this.onNewRooms = onNewRooms;
            this.onNewPeople = onNewPeople;
            this.onMessage = onMessage;
            this.onJoinRoom = onJoinRoom;
            this.onDisconnected = onDisconnected;
            this.listRoomsCb = [];
            this.people = [];
            this.sock = new WebSocket(endpoint);
            this.sock.onopen = function (e) { return _this.onConnected(); };
            this.sock.onclose = function (e) { return _this.onDisconnected(); };
            //this.sock.onerror = e => console.error("Error: " + e);
            this.sock.onmessage = function (e) { return _this.handleMessage(e); };
        }
        Networking.prototype.authenticate = function (name) {
            this.myName = name;
            this.send(1 /* authenticate */, { name: name });
        };
        Networking.prototype.whoAmI = function () {
            return this.myName;
        };
        Networking.prototype.listRooms = function () {
            this.send(2 /* listRooms */);
        };
        Networking.prototype.joinRoom = function (name) {
            this.send(4 /* joinRoom */, { room: name });
        };
        Networking.prototype.sendMessage = function (content) {
            this.send(8 /* sendMessage */, { message: JSON.stringify(content) });
        };
        Networking.prototype.send = function (msgType, data) {
            this.sock.send(msgType.toString() + '~' + JSON.stringify(data || {}));
        };
        Networking.prototype.handleMessage = function (msg) {
            var incomingAll = JSON.parse(msg.data);
            if (!incomingAll)
                console.warn("Invalid JSON data received!");
            //console.log(incomingAll);	// DEBUG
            var incoming = incomingAll.data;
            switch (incomingAll.type) {
                case 3 /* roomsList */:
                    return this.onNewRooms(incoming.rooms);
                case 5 /* peopleList */:
                    this.people = incoming.people;
                    this.onJoinRoom();
                    return this.onNewPeople(this.people);
                case 6 /* personIncoming */:
                    this.people.push(incoming.name);
                    this.people = this.people.sort();
                    return this.onNewPeople(this.people);
                case 7 /* personLeaving */:
                    this.people.splice(this.people.indexOf(incoming.name), 1);
                    return this.onNewPeople(this.people);
                case 9 /* incomingMessage */:
                    return this.onMessage(incoming.sender, JSON.parse(incoming.content));
            }
        };
        return Networking;
    })();
    letscard.Networking = Networking;
})(letscard || (letscard = {}));
var letscard;
(function (letscard) {
    var progId = 0;
    var randId = Math.round(Math.random() * 89999 + 10000);
    var zIndex = 10;
    var Card = (function () {
        function Card(el, game, value, onMove, onUncover) {
            var _this = this;
            this.el = el;
            this.game = game;
            this.value = value;
            this.onMove = onMove;
            this.onUncover = onUncover;
            this.makeDraggable(el.get(0));
            this.id = new Date().getTime() + '_' + (progId++) + '_' + randId + '_' + Math.round(Math.random() * 89999 + 10000);
            this.el.css({
                backgroundImage: 'url("res/cards/' + this.game.cardBackground + '")'
            });
            el.click(function (e) {
                if (!e.ctrlKey)
                    return;
                _this.uncover(e.shiftKey);
            });
        }
        Card.prototype.uncover = function (globally, report) {
            if (report === void 0) { report = true; }
            this.el.removeClass('covered');
            this.el.css({
                backgroundImage: 'url("res/cards/' + this.game.cardsPrefix + this.value + this.game.cardsPostfix + '")'
            });
            if (globally) {
                this.uncovered = true;
                if (report)
                    this.onUncover();
            }
        };
        Card.prototype.isGloballyUnconvered = function () {
            return this.uncovered;
        };
        Card.prototype.moveTo = function (position) {
            this.el.css(position);
            this.el.css('z-index', zIndex++);
        };
        Card.prototype.getPosition = function () {
            return {
                left: parseInt(this.el.css('left')),
                top: parseInt(this.el.css('top'))
            };
        };
        Card.prototype.onUpdatePosition = function (left, top, drop) {
            this.onMove(left, top, drop);
        };
        Card.prototype.makeDraggable = function (el) {
            var _startX = 0; // mouse starting positions
            var _startY = 0;
            var _offsetX = 0; // current element offset
            var _offsetY = 0;
            var _dragElement; // needs to be passed from OnMouseDown to OnMouseMove
            //var _oldZIndex = 0;         // we temporarily increase the z-index during drag
            //var _debug = $('#debug').get(0);    // makes life easier
            var that = this;
            function ExtractNumber(value) {
                var n = parseInt(value);
                return n == null || isNaN(n) ? 0 : n;
            }
            function OnMouseMove(e) {
                if (e == null)
                    e = window.event;
                // this is the actual "drag code"
                _dragElement.style.left = (_offsetX + e.clientX - _startX) + 'px';
                _dragElement.style.top = (_offsetY + e.clientY - _startY) + 'px';
                //_debug.innerHTML = '(' + _dragElement.style.left + ', ' +
                //_dragElement.style.top + ')';
                if (parseInt(_dragElement.style.left) < 0)
                    _dragElement.style.left = '0px';
                else if (parseInt(_dragElement.style.left) > that.el.parent().width() - that.el.outerWidth())
                    _dragElement.style.left = (that.el.parent().width() - that.el.outerWidth()) + 'px';
                if (parseInt(_dragElement.style.top) < 0)
                    _dragElement.style.top = '0px';
                else if (parseInt(_dragElement.style.top) > that.el.parent().height() - that.el.outerHeight())
                    _dragElement.style.top = (that.el.parent().height() - that.el.outerHeight()) + 'px';
                that.onUpdatePosition(parseInt(_dragElement.style.left), parseInt(_dragElement.style.top), false);
            }
            function OnMouseDown(e) {
                // IE is retarded and doesn't pass the event object
                if (e == null)
                    e = window.event;
                // IE uses srcElement, others use target
                var target = e.target != null ? e.target : e.srcElement;
                //_debug.innerHTML = target.className == 'drag'
                //	? 'draggable element clicked'
                //	: 'NON-draggable element clicked';
                // for IE, left click == 1
                // for Firefox, left click == 0
                if ((e.button == 1 && window.event != null || e.button == 0) && target == el) {
                    // grab the mouse position
                    _startX = e.clientX;
                    _startY = e.clientY;
                    // grab the clicked element's position
                    _offsetX = ExtractNumber(target.style.left);
                    _offsetY = ExtractNumber(target.style.top);
                    // bring the clicked element to the front while it is being dragged
                    //_oldZIndex = target.style.zIndex;
                    target.style.zIndex = 10000;
                    $(target).addClass('dragging');
                    // we need to access the element in OnMouseMove
                    _dragElement = target;
                    // tell our code to start moving the element with the mouse
                    document.onmousemove = OnMouseMove;
                    // cancel out any text selections
                    document.body.focus();
                    // prevent text selection in IE
                    document.onselectstart = function () {
                        return false;
                    };
                    // prevent IE from trying to drag an image
                    target.ondragstart = function () {
                        return false;
                    };
                    // prevent text selection (except IE)
                    return false;
                }
            }
            function OnMouseUp(e) {
                if (_dragElement != null) {
                    _dragElement.style.zIndex = (zIndex++).toString(); //_oldZIndex.toString();
                    $(_dragElement).removeClass('dragging');
                    that.onUpdatePosition(parseInt(_dragElement.style.left), parseInt(_dragElement.style.top), true);
                    // we're done with these events until the next OnMouseDown
                    document.onmousemove = null;
                    document.onselectstart = null;
                    _dragElement.ondragstart = null;
                    // this is how we know we're not dragging      
                    _dragElement = null;
                }
            }
            function InitDragDrop() {
                $(document).mousedown(OnMouseDown);
                $(document).mouseup(OnMouseUp);
            }
            InitDragDrop();
        };
        return Card;
    })();
    letscard.Card = Card;
})(letscard || (letscard = {}));
var letscard;
(function (letscard) {
    var Deck = (function () {
        function Deck(left, top, count, allowRepetitions) {
            if (allowRepetitions === void 0) { allowRepetitions = false; }
            this.left = left;
            this.top = top;
            this.count = count;
            this.allowRepetitions = allowRepetitions;
            this.requiredCards = null;
            this.allowedCards = null; // null = all cards
        }
        Deck.prototype.addBaseCardsInterval = function (min, max, repetitions) {
            if (repetitions === void 0) { repetitions = 1; }
            if (!this.requiredCards)
                this.requiredCards = [];
            for (var r = 0; r < repetitions; r++)
                for (var i = min; i <= max; i++)
                    this.requiredCards.push(i);
            return this;
        };
        Deck.prototype.addBaseCards = function () {
            var cards = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                cards[_i - 0] = arguments[_i];
            }
            if (!this.requiredCards)
                this.requiredCards = [];
            for (var i in arguments)
                this.requiredCards.push(arguments[i]);
            return this;
        };
        Deck.prototype.addCardsInterval = function (min, max, repetitions) {
            if (repetitions === void 0) { repetitions = 1; }
            if (!this.allowedCards)
                this.allowedCards = [];
            for (var r = 0; r < repetitions; r++)
                for (var i = min; i <= max; i++)
                    this.allowedCards.push(i);
            return this;
        };
        Deck.prototype.addCards = function () {
            var cards = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                cards[_i - 0] = arguments[_i];
            }
            if (!this.allowedCards)
                this.allowedCards = [];
            for (var i in arguments)
                this.allowedCards.push(arguments[i]);
            return this;
        };
        return Deck;
    })();
    letscard.Deck = Deck;
    var GameSettings = (function () {
        function GameSettings(args) {
            this.cardsCount = 10;
            this.gameName = args.gameName;
            this.cardsPrefix = args.cardsPrefix;
            this.cardsPostfix = args.cardsPostfix;
            this.cardBackground = args.cardBackground || null;
            this.tableImage = args.tableImage || null;
            this.cardClass = args.cardClass || null;
            this.cardWidth = args.cardWidth || null;
            this.cardHeight = args.cardHeight || null;
            this.cardsCount = args.cardsCount;
            this.decks = args.decks;
        }
        return GameSettings;
    })();
    letscard.GameSettings = GameSettings;
})(letscard || (letscard = {}));
/// <reference path="GameSettings" />
var letscard;
(function (letscard) {
    var AvailableGames;
    (function (AvailableGames) {
        AvailableGames.poker = new letscard.GameSettings({
            gameName: "Poker",
            cardsPrefix: 'poker/',
            cardsPostfix: '.png',
            cardBackground: 'poker/back.png',
            cardsCount: 54,
            decks: [
                new letscard.Deck(50 + 100 / 2, 768 - 50 - 150 / 2, 52, false).addCardsInterval(0, 51)
            ]
        });
        AvailableGames.supe = new letscard.GameSettings({
            gameName: "Supe Card Game",
            cardsPrefix: 'supe/',
            cardsPostfix: '.png',
            cardBackground: 'supe/back.png',
            tableImage: 'supe/table.png',
            cardClass: 'letscard-card-supe',
            cardsCount: 26,
            decks: [
                new letscard.Deck(50 + 120 / 2, 768 - 50 - 120 / 2, 50, false).addBaseCardsInterval(0, 21).addBaseCards(22, 23, 24, 24, 25, 25).addCardsInterval(0, 21).addCardsInterval(22, 23, 4).addCardsInterval(24, 25, 8),
                new letscard.Deck(512 + 50 + 120 / 2, 768 - 50 - 120 / 2, 50, false).addBaseCardsInterval(0, 21).addBaseCards(22, 23, 24, 24, 25, 25).addCardsInterval(0, 21).addCardsInterval(22, 23, 4).addCardsInterval(24, 25, 8)
            ]
        });
    })(AvailableGames = letscard.AvailableGames || (letscard.AvailableGames = {}));
})(letscard || (letscard = {}));
/// <reference path="AvailableGames" />
/// <reference path="GameSettings" />
/// <reference path="../lib/jquery.d.ts" />
var letscard;
(function (letscard) {
    var Table = (function () {
        function Table(el, network) {
            this.el = el;
            this.network = network;
            this.lastSend = new Date();
            this.cards = {};
            el.addClass('letscard-table');
            this.game = letscard.AvailableGames.poker;
        }
        Table.prototype.askForRefresh = function (destination) {
            this.refreshing = true;
            this.network.sendMessage({
                action: 'pleaseRefresh',
                destination: destination
            });
        };
        Table.prototype.setGame = function (game) {
            this.setGame__(game);
            this.network.sendMessage({
                action: 'setGame',
                game: game
            });
            this.clear();
        };
        Table.prototype.clear = function (newGame) {
            if (newGame === void 0) { newGame = true; }
            this.clear__(newGame);
            this.network.sendMessage({
                action: 'clear'
            });
        };
        Table.prototype.processMessage = function (message) {
            //console.log("MESSAGE", message);	// DEBUG
            var card = this.cards[message.card];
            switch (message.action) {
                case 'move':
                    if (card)
                        card.moveTo(message.position);
                    return;
                case 'refreshStop':
                    this.refreshing = false;
                    return;
                case 'spawnRefresh':
                    if (this.refreshing)
                        this.spawnCard__(message.value, message.position, message.card);
                    return;
                case 'spawn':
                    this.spawnCard__(message.value, message.position, message.card);
                    return;
                case 'uncover':
                    if (card)
                        card.uncover(true, false);
                    return;
                case 'setGame':
                    this.setGame__(message.game);
                    return;
                case 'clear':
                    this.clear__();
                    return;
                case 'pleaseRefresh':
                    if (message.destination == this.network.whoAmI())
                        this.pleaseRefresh__();
                    return;
            }
        };
        Table.prototype.setGame__ = function (game) {
            this.game = game;
            this.el.css({ backgroundImage: game.tableImage ? ('url("res/cards/' + game.tableImage + '")') : '' });
        };
        Table.prototype.clear__ = function (newGame) {
            if (newGame === void 0) { newGame = true; }
            this.el.empty();
            this.cards = {};
            if (newGame) {
                for (var i in this.game.decks) {
                    var deck = this.game.decks[i];
                    this.spawnDeck(deck);
                }
            }
        };
        Table.prototype.spawnCard__ = function (value, position, id) {
            var _this = this;
            if (id === void 0) { id = null; }
            var card = new letscard.Card($(document.createElement('div')).addClass('letscard-card covered').appendTo(this.el), this.game, value, function (left, top, drop) { return _this.onCardMove(card, left, top, drop); }, function () { return _this.onUncover(card); });
            if (id)
                card.id = id;
            if (this.game.cardClass)
                card.el.addClass(this.game.cardClass);
            card.moveTo(position);
            card.el.css({ width: this.game.cardWidth, height: this.game.cardHeight });
            this.cards[card.id] = card;
            return card;
        };
        Table.prototype.pleaseRefresh__ = function () {
            this.network.sendMessage({
                action: 'setGame',
                game: this.game
            });
            for (var k in this.cards) {
                this.spawnCardNetwork(this.cards[k], true);
                if (this.cards[k].isGloballyUnconvered())
                    this.onUncover(this.cards[k]);
            }
            this.network.sendMessage({
                action: 'refreshStop'
            });
        };
        Table.prototype.onCardMove = function (card, left, top, drop) {
            var now = new Date();
            if (drop || now.getTime() - this.lastSend.getTime() > 1000 / 30) {
                this.lastSend = now;
                this.network.sendMessage({
                    action: 'move',
                    card: card.id,
                    position: {
                        left: left,
                        top: top
                    }
                });
            }
        };
        Table.prototype.onUncover = function (card) {
            this.network.sendMessage({
                action: 'uncover',
                card: card.id
            });
        };
        Table.prototype.spawnDeck = function (deck) {
            var baseCards = deck.requiredCards || [];
            var cards = deck.allowedCards ? deck.allowedCards.slice(0) : [];
            if (!cards.length)
                for (var i = 0; i < this.game.cardsCount; i++)
                    cards.push(i);
            if (baseCards.length > deck.count)
                return console.error("Trying to generate a deck with less cards than the required number.");
            if (!deck.allowRepetitions && deck.count > baseCards.length + cards.length)
                return console.error("Trying to generate a non-repeating deck bigger than the number of available cards.");
            var deckCards = [];
            for (var i = baseCards.length; i < deck.count; i++) {
                var index = Math.floor(Math.random() * cards.length);
                var value = cards[index];
                if (!deck.allowRepetitions)
                    cards.splice(index, 1);
                deckCards.push(value);
            }
            for (var i = 0; i < deckCards.length; i++) {
                var newCard = this.spawnCard__(deckCards[i], {
                    left: deck.left + i * (deck.left / 1024 - 0.5) - (this.game.cardWidth || 100) / 2,
                    top: deck.top - i * 0.5 - (this.game.cardHeight || 150) / 2
                });
                this.spawnCardNetwork(newCard, false);
            }
        };
        Table.prototype.spawnCardNetwork = function (card, refreshing) {
            if (refreshing === void 0) { refreshing = false; }
            this.network.sendMessage({
                action: refreshing ? 'spawnRefresh' : 'spawn',
                card: card.id,
                value: card.value,
                position: card.getPosition()
            });
        };
        return Table;
    })();
    letscard.Table = Table;
})(letscard || (letscard = {}));
var letscard;
(function (letscard) {
    var Utils;
    (function (Utils) {
        Utils.getQuery = function () {
            var query = {};
            location.search.substr(1).split("&").forEach(function (item) {
                var split = item.split("=");
                query[split[0]] = split[1];
            });
            return query;
        };
    })(Utils = letscard.Utils || (letscard.Utils = {}));
})(letscard || (letscard = {}));
/// <reference path="Networking" />
/// <reference path="Table" />
/// <reference path="Utils" />
/// <reference path="../lib/jquery.d.ts" />
var letscard;
(function (letscard) {
    var Game = (function () {
        function Game(el) {
            var _this = this;
            this.el = el;
            this.network = new letscard.Networking(letscard.Utils.getQuery()['server'] || "ws://sbw2d.mooo.com:8181", function () { return _this.onConnected(); }, function (rooms) { return _this.onNewRooms(rooms); }, function (people) { return _this.onNewPeople(people); }, function (sender, content) { return _this.onMessage(sender, content); }, function () { return _this.onJoinRoom(); }, function () { return _this.onDisconnected(); });
            this.el.text("Connecting...");
        }
        Game.prototype.onConnected = function () {
            var _this = this;
            this.el.empty();
            this.authenticateButton = $(document.createElement('button')).text("Authenticate").click(function () { return _this.authenticate(); }).appendTo(this.el);
            this.table = new letscard.Table($(document.createElement('div')).appendTo(this.el), this.network);
        };
        Game.prototype.authenticate = function () {
            var _this = this;
            var name = prompt("Authenticate...", "Anonymous" + Math.round(10000 + Math.random() * 89999));
            if (!name)
                return;
            this.network.authenticate(name);
            this.authenticateButton.remove();
            this.peopleList = $(document.createElement('div')).prependTo(this.el);
            $(document.createElement('button')).text("Select Game").click(function () {
                var games = [];
                for (var k in letscard.AvailableGames)
                    games.push(k);
                var game = prompt("Select a game...\nAvailable: " + games.join(", ") + ".");
                for (var k in letscard.AvailableGames)
                    if (k == game)
                        _this.table.setGame(letscard.AvailableGames[k]);
            }).prependTo(this.el);
            $(document.createElement('button')).text("New Game").click(function () { return _this.table.clear(); }).prependTo(this.el);
            $(document.createElement('button')).text("Join Room").click(function () { return _this.network.listRooms(); }).prependTo(this.el);
        };
        Game.prototype.onNewRooms = function (rooms) {
            var roomName = prompt("Join or create a room...\nRooms: " + (rooms.length ? rooms.join(", ") : "no rooms available yet") + ".", "MyRoom" + Math.round(10000 + Math.random() * 89999));
            if (roomName)
                this.network.joinRoom(roomName);
        };
        Game.prototype.onNewPeople = function (people) {
            this.peopleList.text("People: " + ["you"].concat(people).join(", ") + ".");
        };
        Game.prototype.onMessage = function (sender, content) {
            //console.log("Message from \"" + sender + "\": ", content);	// DEBUG
            this.table.processMessage(content);
        };
        Game.prototype.onJoinRoom = function () {
            console.log("Join room", this.network.people);
            if (this.network.people.length > 0)
                this.table.askForRefresh(this.network.people[0]);
        };
        Game.prototype.onDisconnected = function () {
            this.el.empty().text("Connection lost!");
        };
        return Game;
    })();
    letscard.Game = Game;
})(letscard || (letscard = {}));
/// <reference path="letscard/Game" />
var letscard;
(function (letscard) {
    letscard.go = function (el) {
        if (!WebSocket)
            return console.error("WebSockets not supported!");
        window.DEBUG_GAME = new letscard.Game($(el));
    };
})(letscard || (letscard = {}));
