/// <reference path="letscard/Game" />

module letscard {

	export var go = function(el: HTMLElement) {
		if (!WebSocket) return console.error("WebSockets not supported!");
		
		(<any>window).DEBUG_GAME = new Game($(el));
	};
}