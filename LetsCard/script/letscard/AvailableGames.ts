/// <reference path="GameSettings" />

module letscard.AvailableGames {
	
	export var poker = new GameSettings({
		gameName: "Poker",
		
		cardsPrefix: 'poker/',
		cardsPostfix: '.png',
		cardBackground: 'poker/back.png',
		
		cardsCount: 54,
		
		decks: [
			new Deck(50 + 100/2, 768 - 50 - 150/2, 52, false).addCardsInterval(0, 51)	// 51 = all except jokers
		]
	});
	
	export var supe = new GameSettings({
		gameName: "Supe Card Game",
		
		cardsPrefix: 'supe/',
		cardsPostfix: '.png',
		cardBackground: 'supe/back.png',
		tableImage: 'supe/table.png',
		
		cardClass: 'letscard-card-supe',
		
		cardsCount: 26,
		
		decks: [
			new Deck(      50 + 120/2, 768 - 50 - 120/2, 50, false).addBaseCardsInterval(0, 21).addBaseCards(22, 23, 24, 24, 25, 25).addCardsInterval(0, 21).addCardsInterval(22, 23, 4).addCardsInterval(24, 25, 8),
			new Deck(512 + 50 + 120/2, 768 - 50 - 120/2, 50, false).addBaseCardsInterval(0, 21).addBaseCards(22, 23, 24, 24, 25, 25).addCardsInterval(0, 21).addCardsInterval(22, 23, 4).addCardsInterval(24, 25, 8)
		]
	});
}
