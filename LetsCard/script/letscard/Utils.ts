
module letscard.Utils {
	
	export var getQuery = function() {
		var query: { [key: string]: string } = {};
		location.search.substr(1).split("&").forEach(
			item => {
				var split = item.split("=");
				query[split[0]] = split[1];
			}
		);
		return query;
	}
}
