
module letscard {

	export class Deck {

		public requiredCards: number[] = null;
		public allowedCards: number[] = null;	// null = all cards
		
		constructor(
			public left: number,
			public top: number,
			public count: number,
			public allowRepetitions: boolean = false
			) { }

		public addBaseCardsInterval(min: number, max: number, repetitions: number = 1): Deck {
			if (!this.requiredCards) this.requiredCards = [];
			for (var r = 0; r < repetitions; r++)
				for (var i = min; i <= max; i++)
					this.requiredCards.push(i);
			return this;
		}
		
		public addBaseCards(...cards: number[]): Deck {
			if (!this.requiredCards) this.requiredCards = [];
			for (var i in arguments)
				this.requiredCards.push(arguments[i]);
			return this;
		}
		
		public addCardsInterval(min: number, max: number, repetitions: number = 1): Deck {
			if (!this.allowedCards) this.allowedCards = [];
			for (var r = 0; r < repetitions; r++)
				for (var i = min; i <= max; i++)
					this.allowedCards.push(i);
			return this;
		}

		public addCards(...cards: number[]): Deck {
			if (!this.allowedCards) this.allowedCards = [];
			for (var i in arguments)
				this.allowedCards.push(arguments[i]);
			return this;
		}
	}

	export class GameSettings {

		public gameName: string;

		public cardsPrefix: string;
		public cardsPostfix: string;
		public cardBackground: string;
		public tableImage: string;

		public cardClass: string;
		public cardWidth: number;
		public cardHeight: number;

		public cardsCount = 10;

		public decks: Deck[];

		constructor(args: {
			gameName: string;

			cardsPrefix: string;
			cardsPostfix: string;
			cardBackground?: string;
			tableImage?: string;

			cardClass?: string;
			cardWidth?: number;
			cardHeight?: number;

			cardsCount: number;

			decks: Deck[];
		}) {
			this.gameName = args.gameName;
			this.cardsPrefix = args.cardsPrefix;
			this.cardsPostfix = args.cardsPostfix;
			this.cardBackground = args.cardBackground || null;
			this.tableImage = args.tableImage || null;
			this.cardClass = args.cardClass || null;
			this.cardWidth = args.cardWidth || null;
			this.cardHeight = args.cardHeight || null;
			this.cardsCount = args.cardsCount;
			this.decks = args.decks;
		}
	}
}
