/// <reference path="Networking" />
/// <reference path="Table" />
/// <reference path="Utils" />
/// <reference path="../lib/jquery.d.ts" />

module letscard {

	export class Game {

		private network: Networking;

		private authenticateButton: JQuery;
		private peopleList: JQuery;
		
		private table: Table;
		
		public constructor(private el: JQuery) {
			this.network = new Networking(
				Utils.getQuery()['server'] || "ws://sbw2d.mooo.com:8181",
				() => this.onConnected(),
				rooms => this.onNewRooms(rooms),
				people => this.onNewPeople(people),
				(sender, content) => this.onMessage(sender, content),
				() => this.onJoinRoom(),
				() => this.onDisconnected()
				);
			
			this.el.text("Connecting...");
		}

		private onConnected() {
			this.el.empty();
			this.authenticateButton = $(document.createElement('button')).text("Authenticate").click(() => this.authenticate()).appendTo(this.el);
			this.table = new Table(
				$(document.createElement('div')).appendTo(this.el),
				this.network
			);
		}
		
		private authenticate() {
			var name = prompt("Authenticate...", "Anonymous" + Math.round(10000 + Math.random() * 89999));
			if (!name) return;
			this.network.authenticate(name);
			this.authenticateButton.remove();
			
			this.peopleList = $(document.createElement('div')).prependTo(this.el);
			$(document.createElement('button')).text("Select Game").click(() => {
				var games: string[] = [];
				for(var k in AvailableGames)
					games.push(k);
				var game = prompt("Select a game...\nAvailable: "+games.join(", ")+".");
				for(var k in AvailableGames)
					if(k == game)
						this.table.setGame((<any>AvailableGames)[k]);
			}).prependTo(this.el);
			$(document.createElement('button')).text("New Game").click(() => this.table.clear()).prependTo(this.el);
			$(document.createElement('button')).text("Join Room").click(() => this.network.listRooms()).prependTo(this.el);
		}
		
		private onNewRooms(rooms: string[]) {
			var roomName = prompt("Join or create a room...\nRooms: " + (rooms.length ? rooms.join(", ") : "no rooms available yet") + ".", "MyRoom" + Math.round(10000 + Math.random() * 89999));
			if (roomName)
				this.network.joinRoom(roomName);
		}

		private onNewPeople(people: string[]) {
			this.peopleList.text("People: " + ["you"].concat(people).join(", ") + ".");
		}

		private onMessage(sender: string, content: Object) {
			//console.log("Message from \"" + sender + "\": ", content);	// DEBUG
			this.table.processMessage(content);
		}
		
		private onJoinRoom() {
			console.log("Join room", this.network.people);
			if(this.network.people.length > 0)
				this.table.askForRefresh(this.network.people[0]);
		}
		
		private onDisconnected() {
			this.el.empty().text("Connection lost!");
		}
	}
}
