
module letscard {

	var progId = 0;
	var randId = Math.round(Math.random() * 89999 + 10000);

	var zIndex = 10;

	export class Card {

		public id: string;

		private uncovered: boolean;
		
		constructor(
			public el: JQuery,
			private game: GameSettings,
			public value: number,
			private onMove: (left: number, top: number, drop: boolean) => void,
			private onUncover: () => void
			) {
			this.makeDraggable(el.get(0));
			this.id = new Date().getTime() + '_' + (progId++) + '_' + randId + '_' + Math.round(Math.random() * 89999 + 10000);
			
			this.el.css({
				backgroundImage: 'url("res/cards/'+ this.game.cardBackground + '")'
			});
			
			el.click(e => {
				if (!e.ctrlKey) return;
				this.uncover(e.shiftKey);
			});
		}

		public uncover(globally: boolean, report: boolean = true) {
			this.el.removeClass('covered');
		
			this.el.css({
				backgroundImage: 'url("res/cards/'+ this.game.cardsPrefix + this.value + this.game.cardsPostfix + '")'
			});
			
			if(globally) {
				this.uncovered = true;
				if(report) this.onUncover();
			}
		}

		public isGloballyUnconvered(): boolean {
			return this.uncovered;
		}
		
		public moveTo(position: { left: number; top: number; }) {
			this.el.css(position);
			this.el.css('z-index', zIndex++);
		}
		
		public getPosition(): ({ left: number; top: number; }) {
			return {
				left: parseInt(this.el.css('left')),
				top: parseInt(this.el.css('top'))
			};
		}

		private onUpdatePosition(left: number, top: number, drop: boolean) {
			this.onMove(left, top, drop);
		}

		private makeDraggable(el: HTMLElement) {
			var _startX = 0;            // mouse starting positions
			var _startY = 0;
			var _offsetX = 0;           // current element offset
			var _offsetY = 0;
			var _dragElement: HTMLElement;           // needs to be passed from OnMouseDown to OnMouseMove
			//var _oldZIndex = 0;         // we temporarily increase the z-index during drag
			//var _debug = $('#debug').get(0);    // makes life easier
		
			var that = this;

			function ExtractNumber(value: any) {
				var n = parseInt(value);

				return n == null || isNaN(n) ? 0 : n;
			}

			function OnMouseMove(e: any) {
				if (e == null)
					e = window.event; 

				// this is the actual "drag code"
				_dragElement.style.left = (_offsetX + e.clientX - _startX) + 'px';
				_dragElement.style.top = (_offsetY + e.clientY - _startY) + 'px';

				//_debug.innerHTML = '(' + _dragElement.style.left + ', ' +
				//_dragElement.style.top + ')';
				
				if (parseInt(_dragElement.style.left) < 0)
					_dragElement.style.left = '0px';
				else if (parseInt(_dragElement.style.left) > that.el.parent().width() - that.el.outerWidth())
					_dragElement.style.left = (that.el.parent().width() - that.el.outerWidth()) + 'px';

				if (parseInt(_dragElement.style.top) < 0)
					_dragElement.style.top = '0px';
				else if (parseInt(_dragElement.style.top) > that.el.parent().height() - that.el.outerHeight())
					_dragElement.style.top = (that.el.parent().height() - that.el.outerHeight()) + 'px';

				that.onUpdatePosition(parseInt(_dragElement.style.left), parseInt(_dragElement.style.top), false);
			}

			function OnMouseDown(e: any) {
				// IE is retarded and doesn't pass the event object
				if (e == null)
					e = window.event; 
		    
				// IE uses srcElement, others use target
				var target = e.target != null ? e.target : e.srcElement;

				//_debug.innerHTML = target.className == 'drag'
				//	? 'draggable element clicked'
				//	: 'NON-draggable element clicked';
		
				// for IE, left click == 1
				// for Firefox, left click == 0
				if ((e.button == 1 && window.event != null ||
					e.button == 0) &&
					target == el) {
					// grab the mouse position
					_startX = e.clientX;
					_startY = e.clientY;
		        
					// grab the clicked element's position
					_offsetX = ExtractNumber(target.style.left);
					_offsetY = ExtractNumber(target.style.top);
		        
					// bring the clicked element to the front while it is being dragged
					//_oldZIndex = target.style.zIndex;
					target.style.zIndex = 10000;
					$(target).addClass('dragging');
		        
					// we need to access the element in OnMouseMove
					_dragElement = target;
		
					// tell our code to start moving the element with the mouse
					document.onmousemove = OnMouseMove;
		        
					// cancel out any text selections
					document.body.focus();
		
					// prevent text selection in IE
					document.onselectstart = function() { return false; };
					// prevent IE from trying to drag an image
					target.ondragstart = function() { return false; };
		        
					// prevent text selection (except IE)
					return false;
				}
			}

			function OnMouseUp(e: any) {
				if (_dragElement != null) {
					_dragElement.style.zIndex = (zIndex++).toString();//_oldZIndex.toString();
					$(_dragElement).removeClass('dragging');

					that.onUpdatePosition(parseInt(_dragElement.style.left), parseInt(_dragElement.style.top), true);
					
					// we're done with these events until the next OnMouseDown
					document.onmousemove = null;
					document.onselectstart = null;
					_dragElement.ondragstart = null;

					// this is how we know we're not dragging      
					_dragElement = null;

					//_debug.innerHTML = 'mouse up';
				}
			}

			function InitDragDrop() {
				$(document).mousedown(OnMouseDown);
				$(document).mouseup(OnMouseUp);
			}
			InitDragDrop();
		}
	}
}
