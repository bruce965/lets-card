/// <reference path="AvailableGames" />
/// <reference path="GameSettings" />
/// <reference path="../lib/jquery.d.ts" />

module letscard {

	export class Table {

		private lastSend = new Date();

		private game: GameSettings;
		private cards: { [key: string]: Card } = {};

		private refreshing: boolean;

		constructor(private el: JQuery, private network: Networking) {
			el.addClass('letscard-table');

			this.game = AvailableGames.poker;
		}

		public askForRefresh(destination: string) {
			this.refreshing = true;

			this.network.sendMessage({
				action: 'pleaseRefresh',
				destination: destination
			});
		}

		public setGame(game: GameSettings): void {
			this.setGame__(game);

			this.network.sendMessage({
				action: 'setGame',
				game: game
			});
			
			this.clear();
		}

		public clear(newGame: boolean = true) {
			this.clear__(newGame);

			this.network.sendMessage({
				action: 'clear'
			});
		}

		public processMessage(message: any): void {
			//console.log("MESSAGE", message);	// DEBUG
			
			var card: Card = this.cards[message.card];

			switch (message.action) {
				case 'move':
					if (card) card.moveTo(message.position); return;
				case 'refreshStop':
					this.refreshing = false; return;
				case 'spawnRefresh':
					if (this.refreshing) this.spawnCard__(message.value, message.position, message.card); return;
				case 'spawn':
					this.spawnCard__(message.value, message.position, message.card); return;
				case 'uncover':
					if (card) card.uncover(true, false); return;
				case 'setGame':
					this.setGame__(message.game); return;
				case 'clear':
					this.clear__(); return;
				case 'pleaseRefresh':
					if (message.destination == this.network.whoAmI()) this.pleaseRefresh__(); return;
			}
		}

		private setGame__(game: GameSettings) {
			this.game = game;

			this.el.css({ backgroundImage: game.tableImage ? ('url("res/cards/' + game.tableImage + '")') : '' });
		}

		private clear__(newGame: boolean = true) {
			this.el.empty();
			this.cards = {};

			if (newGame) {
				for (var i in this.game.decks) {
					var deck = this.game.decks[i];
					this.spawnDeck(deck);
				}
			}
		}

		private spawnCard__(value: number, position: { left: number; top: number; }, id: string = null): Card {
			var card: Card = new Card(
				$(document.createElement('div')).addClass('letscard-card covered').appendTo(this.el),
				this.game,
				value,
				(left, top, drop) => this.onCardMove(card, left, top, drop),
				() => this.onUncover(card)
				);
			if (id) card.id = id;
			if (this.game.cardClass) card.el.addClass(this.game.cardClass);
			card.moveTo(position);
			card.el.css({ width: this.game.cardWidth, height: this.game.cardHeight });
			this.cards[card.id] = card;
			return card;
		}

		private pleaseRefresh__(): void {
			this.network.sendMessage({
				action: 'setGame',
				game: this.game
			});
			
			// TODO: sort by zIndex
			for (var k in this.cards) {
				this.spawnCardNetwork(this.cards[k], true);
				
				if(this.cards[k].isGloballyUnconvered())
					this.onUncover(this.cards[k]);
			}

			this.network.sendMessage({
				action: 'refreshStop'
			});
		}

		private onCardMove(card: Card, left: number, top: number, drop: boolean) {
			var now = new Date();
			if (drop || now.getTime() - this.lastSend.getTime() > 1000 / 30) {
				this.lastSend = now;

				this.network.sendMessage({
					action: 'move',
					card: card.id,
					position: {
						left: left,
						top: top
					}
				});
			}
		}

		private onUncover(card: Card) {
			this.network.sendMessage({
				action: 'uncover',
				card: card.id
			});
		}

		private spawnDeck(deck: Deck) {
			var baseCards = deck.requiredCards || [];
			var cards = deck.allowedCards ? deck.allowedCards.slice(0) : [];
			if (!cards.length)	// all cards if allowed cards is empty
				for (var i = 0; i < this.game.cardsCount; i++)
					cards.push(i);

			if(baseCards.length > deck.count)
				return console.error("Trying to generate a deck with less cards than the required number.");
			
			if (!deck.allowRepetitions && deck.count > baseCards.length + cards.length)
				return console.error("Trying to generate a non-repeating deck bigger than the number of available cards.");
			
			var deckCards: number[] = [];
			
			for (var i = baseCards.length; i < deck.count; i++) {
				var index = Math.floor(Math.random() * cards.length);
				var value = cards[index];
				if (!deck.allowRepetitions) cards.splice(index, 1);
				deckCards.push(value);
			}
			
			for(var i=0; i<deckCards.length; i++) {
				var newCard = this.spawnCard__(deckCards[i], {
					left: deck.left + i * (deck.left / 1024 - 0.5) - (this.game.cardWidth || 100) / 2,
					top: deck.top - i * 0.5 - (this.game.cardHeight || 150) / 2
				});
				this.spawnCardNetwork(newCard, false);
			}
		}

		private spawnCardNetwork(card: Card, refreshing: boolean = false) {
			this.network.sendMessage({
				action: refreshing ? 'spawnRefresh' : 'spawn',
				card: card.id,
				value: card.value,
				position: card.getPosition()
			});
		}
	}
}
