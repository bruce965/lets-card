
module letscard {

	enum MessageType {
		authenticate = 1,	// OUT
		listRooms,			// OUT
		roomsList,			// IN
		joinRoom,			// OUT
		peopleList,			// IN
		personIncoming,		// IN
		personLeaving,		// IN
		sendMessage,		// OUT
		incomingMessage		// IN
	}

	export class Networking {

		private sock: WebSocket;
		private listRoomsCb: Function[] = [];
		
		private myName: string;
		public people: string[] = [];
		
		constructor(
			endpoint: string,
			private onConnected: () => void,
			private onNewRooms: (rooms: string[]) => void,
			private onNewPeople: (people: string[]) => void,
			private onMessage: (sender: string, content: Object) => void,
			private onJoinRoom: () => void,
			private onDisconnected: () => void
		) {
			this.sock = new WebSocket(endpoint);
			this.sock.onopen = e => this.onConnected();
			this.sock.onclose = e => this.onDisconnected();
			//this.sock.onerror = e => console.error("Error: " + e);
			this.sock.onmessage = e => this.handleMessage(e);
		}
		
		public authenticate(name: string) : void {
			this.myName = name;
			this.send(MessageType.authenticate, { name: name });
		}
		
		public whoAmI(): string {
			return this.myName;
		}
		
		public listRooms(): void {
			this.send(MessageType.listRooms);
		}
		
		public joinRoom(name: string): void {
			this.send(MessageType.joinRoom, { room: name });
		}
		
		public sendMessage(content: Object): void {
			this.send(MessageType.sendMessage, { message: JSON.stringify(content) });
		}
		
		private send(msgType: MessageType, data?: Object): void {
			this.sock.send(msgType.toString() + '~' + JSON.stringify(data || {}));
		}
		
		private handleMessage(msg: MessageEvent): void {
			var incomingAll = JSON.parse(msg.data);
			if(!incomingAll)
				console.warn("Invalid JSON data received!");
			
			//console.log(incomingAll);	// DEBUG
			
			var incoming = incomingAll.data;
			
			switch(incomingAll.type) {
			case MessageType.roomsList:
				return this.onNewRooms(incoming.rooms);
			case MessageType.peopleList:
				this.people = incoming.people;
				this.onJoinRoom();
				return this.onNewPeople(this.people);
			case MessageType.personIncoming:
				this.people.push(incoming.name);
				this.people = this.people.sort();
				return this.onNewPeople(this.people);
			case MessageType.personLeaving:
				this.people.splice(this.people.indexOf(incoming.name), 1);
				return this.onNewPeople(this.people);
			case MessageType.incomingMessage:
				return this.onMessage(incoming.sender, JSON.parse(incoming.content));
			}
		}
	}
}
